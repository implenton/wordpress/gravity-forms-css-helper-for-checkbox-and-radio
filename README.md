# Gravity Forms CSS Helper for Checkbox and Radio

If you ever wanted to style the radio and checkbox inputs you know that it is possible using only CSS.

Partly because the generated HTML markup by GF:

```html
<li class="…">
    <input name="…" type="radio" value="First Choice" id="…" />
    <label for="…" id="…">First Choice</label>
</li>
```

support a widely popular technique. There are many variations of this CSS trick but all of them follow these steps:

1. Visually hiding the `input`
2. Styling the `label`'s pseudo-element

There are [plenty of articles](https://www.lockedownseo.com/styling-radio-buttons-in-gravity-forms/) [dealing with](https://ryrog.com/create-custom-radio-buttons-gravity-forms/) [this issue](https://legacy.forums.gravityhelp.com/topic/changing-look-of-radio-buttons). If you are curios about a more step by step article this seems to be a good one [seems to be a good one](https://medium.com/claritydesignsystem/pure-css-accessible-checkboxes-and-radios-buttons-54063e759bb3); you can see the [end result here](https://codepen.io/takeradi/pen/rjJpKK).

## The problem

The described CSS technique is perfect until you [enable the "other" choice](https://docs.gravityforms.com/radio-buttons/#general).

> **Enable “other” choice**
> Check this option to add a text input as the final choice of your radio button field. This allows the user to specify a value that is not a predefined choice.

The problem is that the "other"'s markup is different:

```html
<li class='…'>
    <input name="…" type="radio" value="…" id="…" onfocus="…" />
    <input id="…" name="…" type="text" value="Other" aria-label="Other" onfocus="…" onblur="…" />   
</li>
```

As you can see there is **no** `label` element. If there is no `label` then there is no styling for the input.

## Solution

This plugin modifies the default markup.

*It does 3 things:*

1. Wraps the `input` in a `span` tag
2. Inserts an another `span` tag after the `input`
3. Modifies the `onfocus` attribute for the "other" option

### The new markup

For the radio input:

```diff
<li class="…">
+   <span class="gfield_input gfield_input--radio">
        <input name="…" type="radio" value="First Choice" id="…" />
+       <span class="gfield_input__indicator gfield_input__indicator--radio"></span>
+   </span>
    <label for="…" id="…">First Choice</label>
</li>
```

For the "other" option:

```diff
<li class='…'>
+   <span class="gfield_input gfield_input--radio">
        <input name="…" type="radio" value="…" id="…" onfocus="…" />
+       <span class="gfield_input__indicator gfield_input__indicator--radio"></span>
+   </span>
    <input id="…" name="…" type="text" value="Other" aria-label="Other" onfocus="…" onblur="…" />   
</li>
```

For the checkbox input:

```diff
<li class="…">
+   <span class="gfield_input gfield_input--checkbox">
        <input name="…" type="checkbox" value="First Choice" id="…" />
+       <span class="gfield_input__indicator gfield_input__indicator--checkbox"></span>
+   </span>
    <label for="…" id="…">First Choice</label>
</li>
```

### Styling the inputs

Because now all fields and options follow the same HTML structure

```html
<li>
    <span class="gfield_input gfield_input--radio">
        …
        <span class="gfield_input__indicator gfield_input__indicator--radio"></span>
    </span>
</li>
```

you can use the `span.gfield_input` and `span.gfield_input__indicator` to achieve the same visual result.

If you need some guidance you can [check this article](https://cloudfour.com/thinks/styling-form-elements/) for explanation.

The `span.gfield_input__indicator` might seem superfluous and it is not required for the mentioned technique but it is added for maximum customizability.

### The onfocus attribute

As mentioned this plugin modifies the `onfocus` attribute. You might wonder why.
The answer is because Gravity Forms targets elements using the `prev` and `next` method for targeting DOM elements.

Because we modified the HTML markup there is no previous `input` element and this causes an issue.

The plugin replaces this `prev` and `next` tree traversal method with a more "direct" targeting:

```diff
-jQuery(this).next('input').focus();
+jQuery('#input_1_1_other').focus()

-jQuery(this).prev("input")[0].click();
+jQuery("#choice_1_1_3").click();
```

## Filters

#### Changing the default CSS classes:

##### For the radio inputs:

- `gfcssh/radio/container/class`
- `gfcssh/radio/indicator/class`


##### For the checkbox inputs:

- `gfcssh/checkbox/container/class`
- `gfcssh/checkbox/indicator/class`


#### Changing the indicator element content

- `gfcssh/radio/indicator/content`
- `gfcssh/checkbox/indicator/content`

#### Changing the `onfocus` attribute

- `gfcssh/radio/radio/onfocus`
- `gfcssh/radio/input/onfocus`
