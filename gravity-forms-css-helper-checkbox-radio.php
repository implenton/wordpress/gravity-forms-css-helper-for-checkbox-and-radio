<?php
/*
 * Plugin Name:       Gravity Forms CSS Helper for Checkbox and Radio
 * Description:       Helps with the CSS styling of the radio and checkbox inputs by modifying the default markup
 * Plugin URI:        https://implenton.com/
 * Version:           1.3.0
 * Author:            implenton
 * Author URI:        https://implenton.com/
 * License:           GPLv3
 * License URI:       https://www.gnu.org/licenses/gpl-3.0.txt
 * GitLab Plugin URI: implenton/wordpress/gravity-forms-css-helper-for-checkbox-and-radio
 */

namespace GFCSSHelper;

if ( ! defined( 'WPINC' ) ) {
    die;
}

add_filter( 'gform_field_choice_markup_pre_render', __NAMESPACE__ . '\radio_other_attribute', 10, 4 );
add_filter( 'gform_field_choice_markup_pre_render', __NAMESPACE__ . '\radio_markup', 20, 4 );
add_filter( 'gform_field_choice_markup_pre_render', __NAMESPACE__ . '\checkbox_markup', 30, 4 );

function radio_other_attribute( $choice_markup, $choice, $field, $value ) {
    if ( is_admin_render( $field ) ) {
        return $choice_markup;
    }

    $list_elements = new \DOMDocument();
    $list_elements->loadHTML( $choice_markup );

    $other_radio_inputs = find_element( $list_elements, '//input[@value="gf_other_choice"]' );
    $other_text_inputs  = find_element( $list_elements, '//input[substring(@name,string-length(@name)-string-length("_other")+1)="_other"]' );

    if ( is_any_element( $other_radio_inputs ) ) {
        foreach ( $other_radio_inputs as $input ) {
            $input->setAttribute( 'onfocus', apply_filters( 'gfcssh/radio/radio/onfocus', "jQuery('#input_{$field->formId}_{$field->id}_other').focus()", $choice_markup, $choice, $field, $value ) );
        }
    }

    if ( is_any_element( $other_text_inputs ) ) {
        foreach ( $other_text_inputs as $input ) {
            $last_choice_id    = count( $field->choices ) - 1;
            $search            = 'jQuery(this).prev("input")[0]';
            $replace           = sprintf( 'jQuery("#choice_%s_%s_%s")', $field->formId, $field->id, $last_choice_id );
            $current_attribute = $input->getAttribute( 'onfocus' );
            $onfocus           = str_replace( $search, $replace, $current_attribute );

            $input->setAttribute( 'onfocus', apply_filters( 'gfcssh/radio/input/onfocus', $onfocus, $choice_markup, $choice, $field, $value ) );
        }
    }

    $choice_markup = $list_elements->saveHTML();

    return $choice_markup;
}

function radio_markup( $choice_markup, $choice, $field, $value ) {
    if ( is_admin_render( $field ) ) {
        return $choice_markup;
    }

    $list_elements = new \DOMDocument();
    $list_elements->loadHTML( $choice_markup );

    $radioInputs = find_element( $list_elements, '//input[@type="radio"]' );

    if ( is_any_element( $radioInputs ) ) {
        $container = get_container_element( $list_elements, apply_filters( 'gfcssh/radio/container/class', 'gfield_input gfield_input--radio' ) );
        $indicator = get_indicator_element( $list_elements, apply_filters( 'gfcssh/radio/indicator/class', 'gfield_input__indicator gfield_input__indicator--radio' ), apply_filters( 'gfcssh/radio/indicator/content', '' ) );

        foreach ( $radioInputs as $input ) {
            wrap_input_and_add_indicator( $input, $container, $indicator );
        }
    }

    $choice_markup = $list_elements->saveHTML();

    return $choice_markup;
}

function checkbox_markup( $choice_markup, $choice, $field, $value ) {
    if ( is_admin_render( $field ) ) {
        return $choice_markup;
    }

    $list_elements = new \DOMDocument();
    $list_elements->loadHTML( $choice_markup );

    $checkboxInputs = find_element( $list_elements, '//input[@type="checkbox"]' );

    if ( is_any_element( $checkboxInputs ) ) {
        $container = get_container_element( $list_elements, apply_filters( 'gfcssh/checkbox/container/class', 'gfield_input gfield_input--checkbox' ) );
        $indicator = get_indicator_element( $list_elements, apply_filters( 'gfcssh/checkbox/indicator/class', 'gfield_input__indicator gfield_input__indicator--checkbox' ), apply_filters( 'gfcssh/checkbox/indicator/content', '' ) );

        foreach ( $checkboxInputs as $input ) {
            wrap_input_and_add_indicator( $input, $container, $indicator );
        }
    }

    $choice_markup = $list_elements->saveHTML();

    return $choice_markup;
}

function wrap_input_and_add_indicator( $input, $container, $indicator ) {
    $html = $container->cloneNode();
    $input->parentNode->replaceChild( $html, $input );
    $html->appendChild( $input );
    $html->appendChild( $indicator );

    return $html;
}

function get_container_element( $DOM, $CSS_classes ) {
    $container = $DOM->createElement( 'span' );
    $container->setAttribute( 'class', $CSS_classes );

    return $container;
}

function get_indicator_element( $DOM, $CSS_classes, $content ) {
    $indicator = $DOM->createElement( 'span' );
    $indicator->setAttribute( 'class', $CSS_classes );
    $indicator->textContent = $content;

    return $indicator;
}

function is_admin_render( $field_data ) {
    return $field_data->is_entry_detail() || $field_data->is_form_editor();
}

function find_element( $DOM, $query_expression ) {
    $xpath    = new \DOMXPath( $DOM );
    $elements = $xpath->query( $query_expression );

    return $elements;
}

function is_any_element( $elements ) {
    return ! empty( $elements ) && $elements !== false ? true : false;
}
